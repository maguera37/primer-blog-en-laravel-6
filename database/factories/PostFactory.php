<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

//con este factori lo que hacemos es llenar en la BD datos falsos en las tablas espicificadas en el return de la funcion de abajo
$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,//esa frase me trae sentence->'Sit vitae voluptas sint non voluptates.'
        'body'  => $faker->paragraph,//ese texto me trae paragraph->'Ut ab voluptas sed a nam. Sint autem inventore aut officia aut aut blanditiis. Ducimus eos odit amet et est ut eum.'
        'slug'  => Str::slug($faker->sentence),
        'user_id' => App\User::all()->random()->id,//aquí nos trae un id aleartoreo
        'category_id' => App\Category::all()->random()->id,
        'image' => asset('imganes/posts/'.$faker->image('public/imagenes/posts', 640, 480, null, false))//aqui especificamos en donde se va aguardar la imagen, lo que está en "null" es la categoria y false para que sea una imagen aleartoria, pasamos el asset() para que nos traiga la ruta completa de donde está la imagen
    ];

    //ahora hay que ir a la carpeta seeds y en entrar al archivo DatabaseSeeder.php para indicarle cuanto usuarios va a crear u añadir a la BD
});





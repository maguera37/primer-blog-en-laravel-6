<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        //lo que hago en los siguientes codigo es insertar/llenar en la BD del blog1 en la tabla 'categories' los siguientes datos en especificando en que campo 
        
        DB::table('users')->insert([
            'id'            => '1',
            'name'          => 'Martín',
            'email'         => 'asd@gmail.com',
            'password'      => bcrypt('12345'),
            'created_at'    => now(),
            'updated_at'    => now()
        ]);

        DB::table('categories')->insert([
            'id'            => '1',
            'name'          => 'Novedades',
            'created_at'    => now(),
            'updated_at'    => now()
        ]);

        DB::table('categories')->insert([
            'id'            => '2',
            'name'          => 'Comida',
            'created_at'    => now(),
            'updated_at'    => now()
        ]);

        DB::table('categories')->insert([
            'id'            => '3',
            'name'          => 'Deporte',
            'created_at'    => now(),
            'updated_at'    => now()
        ]);

        DB::table('categories')->insert([
            'id'            => '4',
            'name'          => 'Programación',
            'created_at'    => now(),
            'updated_at'    => now()
        ]);

        //aquí llamo al factory diciendo que me creé 20 Publiaciones
        // factory('App\Post', 20)->create();
    }
}

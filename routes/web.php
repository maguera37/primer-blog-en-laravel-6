<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'FrontController@index');
Route::get('post/{slug}', 'FrontController@show');


//Es para que los usuarios que no están registrados no puedan entrer a dicha vista.
Route::middleware(['auth'])->group(function () { 
    Route::resource('categories', 'CategoryController');
    Route::resource('posts', 'PostController');
});

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    //aquí llenamos los campos que están en la tabla de la BD
    protected $fillable =[
        'name'
    ];

    public function posts(){
        return $this->hasMany('App\Post');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class FrontController extends Controller
{
    public function index(){
        
        $posts = Post::orderBy('id', 'DESC')->paginate(6);
        return view('welcome', compact('posts'));
    }

    public function show($slug){

        //la variable llama a la clase Post cuando el slug sea igual al parametro $slug que le estamos pasando y nos devuelma como primer registro
        $post = Post::where('slug', $slug)->first();
        return view('post', compact('post'));
    }
}

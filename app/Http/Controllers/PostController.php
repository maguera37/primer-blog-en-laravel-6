<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Category;
use App\Post;
use Auth;
use Session;
use Redirect;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //busca por el id en el modelo User al usuario que ya este registrado
        $user = User::find(Auth::User()->id);   

        //traigo todas las categorias
        $categories = Category::all();   
    
        return view('posts.create', compact('categories', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regla = [
            'title' => 'required',
            'body' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg,gif|max:2000'
        ];

        $mensaje = [
            'title.required' => 'Para crear hay que llenar',
            'body.required' => 'Es necesario un contenido   ',
            'image.mimes' => 'El archivo debe corresponder a un formato de imagen',
            'image.max' => 'La imagen no debe ser mayor que 2 MB'
        ];
       
        $this->validate($request, $regla, $mensaje);

        $post = new Post($request->all());

        $post->slug = Str::slug($request->title);
    
        $post->save();

        if($request->file('image')){
            
            //despues de disk indicamos el driver que vamos a usar y en el metodo put idicamos en donde se va almacenar y con que nombre, se almacena en una carpeta que la crea automaticamente laravel pero le indicamos el nombre y se guarda con el nombre original de la imagen
            $nombre = Storage::disk('imaposts')->put('imagenes/posts',$request->file('image'));
            
            $post->fill(['image' => asset($nombre)])->save();
        }

        Session::flash('message', 'Publicación creada');

        return redirect()->route('posts.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        
        $categories = Category::all();

        return view('posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $regla = [
            'title' => 'required',
            'body' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg,gif|max:2000'
        ];

        $mensaje = [
            'title.required' => 'Para crear hay que llenar',
            'body.required' => 'Es necesario un contenido   ',
            'image.mimes' => 'El archivo debe corresponder a un formato de imagen',
            'image.max' => 'La imagen no debe ser mayor que 2 MB'
        ];
       
        $this->validate($request, $regla, $mensaje);

        $post = Post::find($id);

        $post->slug = Str::slug($request->title);
    
        $post->update($request->all());

        if($request->file('image')){
            
            //despues de disk indicamos el driver que vamos a usar y en el metodo put idicamos en donde se va almacenar y con que nombre, se almacena en una carpeta que la crea automaticamente laravel pero le indicamos el nombre y se guarda con el nombre original de la imagen
            $nombre = Storage::disk('imaposts')->put('imagenes/post',$request->file('image'));
            
            $post->fill(['image' => asset($nombre)])->save();
        }

        Session::flash('message', 'Publicación actualizada');

        return redirect()->route('posts.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        
        $post->delete();

        Session::flash('message', 'Publicaión borrada');
        
        return redirect()->route('posts.index');
    }
}

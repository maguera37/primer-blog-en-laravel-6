@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="{{ route('posts.create') }}" class="btn btn-secondary mb-3">Crear Publicaciones</a>
                <div class="card">
                    @if(Session::has('message'))
                        <div class="alert alert-secondary">{{ Session::get('message') }}</div>
                    @endif
                    <div class="card-header">Lista de Publicaciones</div>
                    <table class="table">
                        <thead class="thead-dark">
                          <tr>
                            {{-- <th scope="col">id</th> --}}
                            <th scope="col">Titulo</th>
                            <th scope="col">Categorias</th>
                            {{-- <th scope="col">Autor</th> --}}
                            <th scope="col">Portada</th>
                            <th scope="col">Acción</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    {{-- <th scope="row">{{$post->id}}</th> --}}
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->category->name}}</td>
                                    {{-- <td>{{$post->user->name}}</td> --}}
                                    <td><img src="{{$post->image}}" alt="" width="75" height="75"></td>
                                    <td style="col">
                                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-success mb-2 w-100 ">Editar</a>
                                        <form action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger w-100" onclick="return cofirm('¿Seguro/a?')">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-primary text-white">Crear Publicaciones</div>
                    <form action="{{ route('posts.store') }}" method="POST" class="m-4 text-primary" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="form-group">
                            <label class="form-control-label">Categorias</label>
                            <select name="category_id" class="form-control">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Titulo de la Publicación</label>
                            <input type="text" class="form-control form-control-alternative" name="title" value="{{ old('title') }}">
                            @if($errors->has('title'))
                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                            @endif  
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Contenido</label>
                            <textarea name="body" rows="3" class="form-control" value="{{ old('body') }}"></textarea>
                            @if($errors->has('body'))
                                <strong class="text-danger">{{ $errors->first('body') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Portada</label>
                            <input type="file" class="form-control form-control-alternative" name="image">
                            @if($errors->has('image'))
                                <strong class="text-danger">{{ $errors->first('image') }}</strong>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary m-4">Crear</button>
                    </form>
                </div>
            </div>
        </div>
        <a class="btn btn-dark m-5" href="{{ route('posts.index') }}"><- Volver</a>
    </div>
@endsection
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-success text-white">Editar Publicación</div>
                    <form action="{{ route('posts.update', $post->id) }}" method="POST" class="m-4 text-success" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label class="form-control-label">Categorias</label>
                            <select name="category_id" class="form-control">
                                <option value="{{ $post->category_id }}" selected="selected">{{ $post->category->name }}</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Titulo de la Publicación</label>
                            <input type="text" class="form-control form-control-alternative" name="title" value="{{ $post->title }}">
                            @if($errors->has('title'))
                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                            @endif  
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Contenido</label>
                            <textarea name="body" rows="3" class="form-control">{{ $post->body }}</textarea>
                            @if($errors->has('body'))
                                <strong class="text-danger">{{ $errors->first('body') }}</strong>
                            @endif
                        </div>
                        <img src="{{ $post->image }}" alt="" class="img-fluid" width="200" height="300">
                        <div class="form-group">
                            <label class="form-control-label">Portada</label>
                            <input type="file" class="form-control form-control-alternative" name="image" value="{{ $post->image }}">
                            @if($errors->has('image'))
                                <strong class="text-danger">{{ $errors->first('image') }}</strong>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-success m-4">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
        <a class="btn btn-dark m-5" href="{{ route('posts.index') }}"><- Volver</a>
    </div>
@endsection
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-primary text-white">Crear Categoria</div>
                    <form action="{{ route('categories.store') }}" method="POST" class="m-4">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="form-control-label">Nombre de la Categoria</label>
                            <input type="text" name="name" class="form-control form-control-alternative">
                            @if($errors->has('name'))
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary m-4">Crear</button>
                    </form>
                </div>
            </div>
        </div>
        <a class="btn btn-dark m-5" href="{{ route('categories.index') }}"><- Volver</a>
    </div>
@endsection
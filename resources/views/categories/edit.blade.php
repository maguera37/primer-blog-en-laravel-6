@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-success text-white">Editar Categoria</div>
                    <form action="{{ route('categories.update', $category->id) }}" method="POST" class="m-4">
                        @csrf
                        @method('PUT'){{-- lo usamos porque estamos por actualizar el registro --}}
                        <div class="form-group">
                            <label for="name" class="form-control-label">Nombre de la Categoria</label>
                            <input type="text" name="name" class="form-control form-control-alternative" value="{{ $category->name }}">
                            @if($errors->has('name'))
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-success m-4">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
        <a class="btn btn-dark m-5" href="{{ route('categories.index') }}"><- Volver</a>
    </div>
@endsection